﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FileProduct = IReckonu_HafizAdewuyi.Models.File.Product;
using DataProduct = IReckonu_HafizAdewuyi.DataStorage.Models.Product;
using IReckonu_HafizAdewuyi.DataStorage;
using IReckonu_HafizAdewuyi.Models.File;
using IReckonu_HafizAdewuyi.Exceptions;

namespace IReckonu_HafizAdewuyi.Services
{
    public class ProductService : IProductService
    {
        private IProductRepository _productRepository;
        private int RowsPerSave = 10000; //TODO: Make configurable

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task ImportProductsFromFile(string filePath)
        {
            //TODO: Cater for non-comma delimeters and cells with values containing commas
            string batchReference = Guid.NewGuid().ToString();
            IEnumerable<string> headers = File.ReadLines(filePath).First().Split(',').Select(cell => cell.Trim());
            IList<PropertyInfo> orderedProperties = headers
                .Select(h =>
                {
                    PropertyInfo headerProp = typeof(FileProduct).GetProperty(h);
                    if (headerProp == null)
                    {
                        throw new UserFriendlyException("The file is not in a correct format");
                    }
                    return headerProp;
                })
                .ToList();

            List<DataProduct> batchProducts = new List<DataProduct>();

            // Save file to data stores
            foreach (var line in File.ReadLines(filePath).Skip(1)) // Skip the header
            {
                FileProduct fileProduct = new FileProduct();
                IList<string> rowCells = line.Split(',').Select(cell => cell.Trim()).ToList();
                for (int i = 0; i < rowCells.Count(); i++)
                {
                    orderedProperties[i].SetValue(fileProduct, rowCells[i]);
                }

                fileProduct.ImportBatchRef = batchReference;

                batchProducts.Add(MapFileToDataModel(fileProduct));

                // Save in batches to prevent holding too much data in memory
                if (batchProducts.Count == RowsPerSave)
                {
                    await SaveProducts(batchProducts);
                    batchProducts.Clear();
                }
            }

            if (batchProducts.Count > 0)
            {
                await SaveProducts(batchProducts);
            }
        }

        private DataProduct MapFileToDataModel(FileProduct fileProduct)
        {
            return new DataProduct
            {
                ArticleCode = fileProduct.ArtikelCode,
                Color = fileProduct.Color,
                ColorCode = fileProduct.ColorCode,
                DeliveredIn = fileProduct.DeliveredIn,
                Description = fileProduct.Description,
                DiscountPrice = fileProduct.DiscountPrice,
                Key = fileProduct.Key,
                Price = fileProduct.Price,
                Q1 = fileProduct.Q1,
                Size = fileProduct.Size,
                ImportBatchRef = fileProduct.ImportBatchRef
            };
        }

        public async Task SaveProduct(DataProduct product)
        {
            await _productRepository.SaveItem(product);
        }

        public async Task SaveProducts(IList<DataProduct> products)
        {
            await _productRepository.SaveItems(products);
        }

        public async Task SaveProduct(FileProduct product)
        {
            await SaveProduct(MapFileToDataModel(product));
        }

        public async Task SaveProducts(IList<FileProduct> products)
        {
            await SaveProducts(products.ToList().Select(product => MapFileToDataModel(product)).ToList());
        }
    }
}
