﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using FileProduct = IReckonu_HafizAdewuyi.Models.File.Product;
using DataProduct = IReckonu_HafizAdewuyi.DataStorage.Models.Product;
using IReckonu_HafizAdewuyi.DataStorage;

namespace IReckonu_HafizAdewuyi.Services
{
    public interface IProductService
    {
        Task ImportProductsFromFile(string filePath);

        Task SaveProduct(FileProduct product);

        Task SaveProducts(IList<FileProduct> product);
    }
}
