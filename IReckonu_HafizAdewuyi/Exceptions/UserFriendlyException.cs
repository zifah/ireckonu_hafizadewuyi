﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.Exceptions
{
    public class UserFriendlyException : Exception
    {
        public UserFriendlyException() : base()
        {

        }

        public UserFriendlyException(string message) : base(message)
        {

        }
    }
}
