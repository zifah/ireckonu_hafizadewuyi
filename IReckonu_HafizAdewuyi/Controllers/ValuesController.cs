﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IReckonu_HafizAdewuyi.Utilities;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace IReckonu_HafizAdewuyi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        [HttpPost]
        [DisableFormValueModelBinding]
        [RequestSizeLimit(21_474_836_480)]
        public async Task<IActionResult> Index()
        {
            FormValueProvider formModel;
            using (var stream = System.IO.File.Create("c:\\temp\\myfile.temp"))
            {
                formModel = await Request.StreamFile(stream);
            }

            return Ok("File uploaded successfully");
        }
    }
}
