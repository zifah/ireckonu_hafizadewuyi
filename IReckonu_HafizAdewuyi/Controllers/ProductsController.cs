﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using IReckonu_HafizAdewuyi.Models.File;
using IReckonu_HafizAdewuyi.Models.HTTP;
using IReckonu_HafizAdewuyi.Services;
using IReckonu_HafizAdewuyi.Utilities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace IReckonu_HafizAdewuyi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private const long MaxFileUploadSizeBytes = 21_474_836_480; // 20 Gigabytes

        private IProductService _productService;

        public ProductsController(IProductService productService)
        {
            _productService = productService;
        }

        // GET: api/Products
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            return BadRequest("The API does not support fetching a list of products yet.");
        }

        /// <summary>
        /// GET: api/Products/00000002groe56
        /// </summary>
        /// <param name="key">The product key</param>
        /// <returns></returns>
        // 
        [HttpGet("{key}")]
        public async Task<IActionResult> Get(string key)
        {
            return BadRequest("The API does not support fetching a single product yet.");
        }

        // POST: /
        public async Task<IActionResult> Post([FromBody]Product request)
        {
            await _productService.SaveProduct(request);
            return Ok();
        }

        // POST: api/Products/UploadFromURL
        [HttpPost("UploadFromURL")]
        public async Task<IActionResult> Post([FromForm]ProductsUploadFromURLRequest request)
        {
            // TODO: Implement upload from file URL
            return Ok(request.URL);
        }

        [HttpPost("Upload")]
        [DisableFormValueModelBinding]
        [RequestSizeLimit(MaxFileUploadSizeBytes)]
        public async Task<IActionResult> Post()
        {
            FormValueProvider formModel;
            string importedFilePath = $"{Path.GetTempPath()}\\Products-{DateTime.UtcNow.ToString("YYYYMMDD-HHmmss")}.csv";
            using (var stream = System.IO.File.Create(importedFilePath))
            {
                formModel = await Request.StreamFile(stream);
            }

            await _productService.ImportProductsFromFile(importedFilePath);

            return Ok("File uploaded successfully");
        }
    }
}
