﻿using IReckonu_HafizAdewuyi.Exceptions;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private const string UNHANDLED_EXCEPTION_MESSAGE = "A system error has occurred!";

        private readonly RequestDelegate _next;
        private readonly ILogger _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            this._next = next;
            _logger = loggerFactory.CreateLogger<ErrorHandlingMiddleware>();
        }

        public async Task Invoke(HttpContext context /* other dependencies */)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var code = HttpStatusCode.InternalServerError; // 500 if unexpected
            string message = string.Empty;
            string result;

            if (exception is UserFriendlyException)
            {
                result = JsonConvert.SerializeObject(new { error = exception.Message });
            }
            else
            {
                message = UNHANDLED_EXCEPTION_MESSAGE;
                result = JsonConvert.SerializeObject(new { error = message });
            }

            _logger.LogError(exception, message);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = (int)code;
            return context.Response.WriteAsync(result);
        }
    }
}
