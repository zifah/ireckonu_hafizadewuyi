﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.Models.HTTP
{
    public class ProductsUploadFromURLRequest
    {
        public string URL { get; set; }
        public string UserEmail { get; set; }
        public string FirstName { get; set; }
    }
}
