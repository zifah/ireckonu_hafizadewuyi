﻿using IReckonu_HafizAdewuyi.DataStorage.Models;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    // TODO: Instantiate configuration for file storage location here, and some generic methods (Save, SaveMany)
    public class JSONFileDbContext : AbstractDbContext
    {
        private readonly string _connectionString;
        public JSONFileDbContext(IOptions<DatastoreConfig> datastoreConfig)
        {
            _connectionString = GetConnectionString(datastoreConfig);
        }

        public async Task Save<T>(T entity) where T : Entity
        {
            // TODO: Implement lock on this method because of filesystem single write-access restriction
            string filePath = GetFilestorePath<T>();
            string prefix = GetAppendPrefix<T>(filePath);
            await File.AppendAllTextAsync(filePath, prefix + JsonConvert.SerializeObject(entity, Formatting.None));
        }

        public async Task SaveMany<T>(IList<T> entities) where T : Entity
        {
            string filePath = GetFilestorePath<T>();
            string prefix = GetAppendPrefix<T>(filePath);
            string toAppend = JsonConvert.SerializeObject(entities, Formatting.None).TrimStart('[').TrimEnd(']');
            await File.AppendAllTextAsync(filePath, prefix + toAppend);
        }

        private string GetFilestorePath<T>() where T : Entity
        {
            string entityFileName = $"{typeof(T).Name}.json";
            return $"{_connectionString}/{entityFileName}";
        }

        private string GetAppendPrefix<T>(string filestorePath)
        {
            if (!File.Exists(filestorePath) || new FileInfo(filestorePath).Length == 0)
            {
                return string.Empty;
            }
            return $",";
        }
    }
}
