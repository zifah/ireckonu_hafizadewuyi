﻿using IReckonu_HafizAdewuyi.DataStorage.Models;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.DependencyInjection;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    public class MongoDatastore<T> : ISingleDatastore<T> where T : Entity
    {
        private MongoDbContext _dbContext;

        public MongoDatastore(IServiceProvider serviceProvider)
        {
            _dbContext = serviceProvider.GetService<MongoDbContext>();
        }

        public async Task SaveItem(T item)
        {
            await _dbContext.Save(item);
        }

        public async Task SaveItems(IList<T> items)
        {
            await _dbContext.SaveMany(items);
        }
    }
}
