﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage.Models
{
    public class Entity
    {
        public Entity()
        {
            CreatedDate = DateTime.Now;
        }

        public DateTime CreatedDate { get; set; }
    }
}
