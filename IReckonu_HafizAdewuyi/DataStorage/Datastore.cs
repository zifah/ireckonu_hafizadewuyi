﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    public class Datastore
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public string ConnectionString { get; set; }
    }
}
