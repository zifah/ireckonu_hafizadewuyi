﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    public abstract class AbstractDbContext
    {
        public string GetConnectionString(IOptions<DatastoreConfig> datastoreConfig)
        {
            return datastoreConfig
                .Value
                .Datastores
                .SingleOrDefault(s => s.IsActive && s.Name.ToLower() == this.GetType().Name.ToLower().Replace("dbcontext", string.Empty))
                .ConnectionString;
        }
    }
}
