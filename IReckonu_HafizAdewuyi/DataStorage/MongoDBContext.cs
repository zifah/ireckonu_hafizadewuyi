﻿using IReckonu_HafizAdewuyi.DataStorage.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    // TODO: Instantiate configuration for file storage location here, and some generic methods (Save, SaveMany)
    public class MongoDbContext : AbstractDbContext
    {
        private readonly IMongoDatabase _database;
        public MongoDbContext(IOptions<DatastoreConfig> datastoreConfig)
        {
            string connectionString = GetConnectionString(datastoreConfig);
            var _databaseName = MongoUrl.Create(connectionString).DatabaseName;
            _database = new MongoClient(connectionString).GetDatabase(_databaseName);
        }

        public async Task Save<T>(T entity) where T : Entity
        {
            await GetCollectionName<T>().InsertOneAsync(entity);
        }

        public async Task SaveMany<T>(IList<T> entities) where T : Entity
        {
            await GetCollectionName<T>().InsertManyAsync(entities);
        }

        private IMongoCollection<T> GetCollectionName<T>() where T : Entity
        {
            return _database.GetCollection<T>(typeof(T).Name);
        }
    }
}
