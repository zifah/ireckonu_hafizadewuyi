﻿using IReckonu_HafizAdewuyi.DataStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    /// <summary>
    /// Stores data to a single datastore type e.g FileSystem, MongoDB
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ISingleDatastore<T> : IDatastore<T> where T : Entity
    {
    }
}
