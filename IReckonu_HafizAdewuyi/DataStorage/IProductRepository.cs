﻿using IReckonu_HafizAdewuyi.DataStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    public interface IProductRepository : IDatastore<Product>
    {
    }
}
