﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using IReckonu_HafizAdewuyi.DataStorage.Models;
using IReckonu_HafizAdewuyi.Utilities;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    /// <summary>
    /// Saves products to all configured data stores that are active
    /// </summary>
    public class ProductRepository : IProductRepository
    {
        private readonly List<ISingleDatastore<Product>> _datastores;
        private readonly ILogger _logger;

        public ProductRepository(IOptions<DatastoreConfig> datastoreConfig, IServiceProvider serviceProvider, ILoggerFactory loggerFactory)
        {
            // Create an instance of each active data store
            _datastores = new List<ISingleDatastore<Product>>();
            var activateDatastores = datastoreConfig.Value.Datastores.Where(c => c.IsActive);
            List<Type> singleDatastores = ReflectionHelper.GetAllTypesImplementingOpenGenericType(typeof(ISingleDatastore<>), Assembly.GetExecutingAssembly()).ToList();
            Type[] tArgs = { typeof(Product) };
            singleDatastores.ForEach(storeType =>
            {
                string storeName = storeType.Name.Remove(storeType.Name.IndexOf('`')).ToLower().Replace("datastore", string.Empty);
                if (activateDatastores.Select(d => d.Name.ToLower()).Contains(storeName))
                {
                    Type target = storeType.MakeGenericType(tArgs);
                    _datastores.Add((ISingleDatastore<Product>)Activator.CreateInstance(target, serviceProvider));
                }
            });

            _logger = loggerFactory.CreateLogger<ProductRepository>();
        }

        public async Task SaveItem(Product item)
        {
            await Task.Run(() =>
            {
                Parallel.ForEach(_datastores, async store =>
                {
                    try
                    {
                        await store.SaveItem(item);
                        _logger.LogInformation($"Finished storing with ${store.GetType().Name}");

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Failed to save data with ${store.GetType().Name}");
                    }
                });
            });
        }

        public async Task SaveItems(IList<Product> items)
        {
            await Task.Run(() =>
            {
                Parallel.ForEach(_datastores, async store =>
                {
                    try
                    {
                        await store.SaveItems(items);
                        _logger.LogInformation($"Finished storing with ${store.GetType().Name}");

                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Failed to save data with ${store.GetType().Name}");
                    }
                });
            });
        }
    }
}
