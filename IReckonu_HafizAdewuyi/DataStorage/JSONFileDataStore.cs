﻿using IReckonu_HafizAdewuyi.DataStorage.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;

namespace IReckonu_HafizAdewuyi.DataStorage
{
    public class JSONFileDatastore<T> : ISingleDatastore<T> where T : Entity
    {
        private JSONFileDbContext _dbContext;

        public JSONFileDatastore(IServiceProvider serviceProvider)
        {
            _dbContext = serviceProvider.GetService<JSONFileDbContext>();
        }

        public async Task SaveItem(T item)
        {
            await _dbContext.Save(item);
        }

        public async Task SaveItems(IList<T> items)
        {
            await _dbContext.SaveMany(items);
        }
    }
}
