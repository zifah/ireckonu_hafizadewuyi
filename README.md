# Technical Challenge - File importation and storage

## Introduction
This .NET core Web API accepts a .csv file containing a table of products (via HTTP upload), parses the file and saves the products therein to a:
- File in JSON format
- MongoDB collection
The products imported from the same file are tagged with an additional field called `ImportBatchRef` for tracking purposes.

## How to run on development environment
1. Configure a JSON file store (disk folder) and Mongo DB data store which are accessible from the application host server in the `Datastorage.Datastores` section of the `appsettings.development.json` file. Either or both of the datastores can be disabled using the `IsActive` flag of the datastore setting

2. Run the application using the `IReckonu_HafizAdewuyi` launch profile to open the barebones file-upload testing page

3. Upload the products file in the specified format with columns: **Key**,**ArtikelCode**,**DiscountPrice**,**DeliveredIn**,**Q1**,**Size**,**Color**,**ColorCode**,**Description**,**Price**) in any order. 

The response page should display `File uploaded successfully` if everything went fine (verify by checking the configured JSON file store and Mongo DB data store). Otherwise, an error message will be displayed. Check the output and console to see more information on the error.

